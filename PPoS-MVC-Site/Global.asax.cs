﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace PPoS_MVC_Site
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);

            RouteTable.Routes.MapRoute(
                name: "Day",
                url: "Sales/Day/{d}",
                defaults: new { controller = "Sales", action = "Day", d = RouteParameter.Optional }
            );

            RouteTable.Routes.MapRoute(
                name: "Month",
                url: "Sales/Month/{d}",
                defaults: new { controller = "Sales", action = "Month", d = RouteParameter.Optional }
            );

            RouteTable.Routes.MapRoute(
                name: "Year",
                url: "Sales/Year/{d}",
                defaults: new { controller = "Sales", action = "Year", d = RouteParameter.Optional }
            );
            
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

        }
    }
}