﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PPoS_MVC_Site.Models;

namespace PPoS_MVC_Site.ViewModels
{
    public class DataDayViewModel
    {
        public DateTime targetDate { get; set; }

        public int numberOfCashSales { get; set; }
        public decimal totalCash { get; set; }
        public int numberOfCardSales { get; set; }
        public decimal totalCard { get; set; }
        public int numberOfChequeSales { get; set; }
        public decimal totalCheque { get; set; }
        public int numberOfLoyaltySales { get; set; }
        public decimal totalLoyalty { get; set; }
        public int numberOfChargeSales { get; set; }
        public decimal totalCharge { get; set; }
        public decimal totalGrand { get; set; }
    }
}