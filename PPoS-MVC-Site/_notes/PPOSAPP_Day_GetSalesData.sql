﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ===============================================================
-- Author:		John Cogan
-- Create date: 31 July 2013
-- Description:	Get sales figures from Sales Journal for the day
-- ===============================================================
CREATE PROCEDURE PPOSAPP_Day_GetSalesData
	@FuncKey int,
	@SalesDateTime datetime
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from interfering with SELECT statements.
	SET NOCOUNT ON;

    SELECT *
	FROM Sales_Journal 
	WHERE Function_Key = @FuncKey AND 
	CONVERT(DATETIME, FLOOR(CONVERT(FLOAT, Date_Time))) = @SalesDateTime
	ORDER BY Date_Time ASC
END
GO
