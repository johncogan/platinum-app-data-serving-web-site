﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PPoS_MVC_Site.Models;
using System.Data.Objects;
using PPoS_MVC_Site.ViewModels;
using PPoS_MVC_Site.Models.Repositories;
using System.IO;

namespace PPoS_MVC_Site.Controllers
{
    public class SalesController : Controller
    {
        private PlatinumContext db = new PlatinumContext();
        //private SalesRepository salesRepo;

        private DateTime dtTargetDate;

        private SalesRepository salesRepo;

        private string webConfigPassPhrase;

        private int ignoringPassphrase;

        public SalesController() {
            // Disable proxy creation in order to disable lazy loading (To avoid circular reference errors)
            db.Configuration.ProxyCreationEnabled = false;
            System.Configuration.AppSettingsReader appReader = new System.Configuration.AppSettingsReader();

            this.webConfigPassPhrase = appReader.GetValue("passphrase", typeof(string)).ToString();
            this.ignoringPassphrase = int.Parse(appReader.GetValue("ignorePassphrase", typeof(string)).ToString());
        }

        public ViewResult Index()
        {
            return View();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="form">FormCollection</param>
        /// <returns>JsonResult</returns>
        [HttpGet]
        public JsonResult Day(string d, string p)
        {
            DayData dbData = new DayData();
            SalesRepository.Log("New request", "Request type is Day Data");

            string dateDay = this.RouteData.Values["d"].ToString();
            string formDataDateTime = dateDay.Replace("-", "/");

            string passPhrase = null;
            int error = 0;
            string exceptionMessage = null;
            String dataReturnedToClient = @"";

            SalesRepository.Log("dateDay", dateDay.ToString());

            passPhrase = this.HttpContext.Request.Headers["p"];

            if (passPhrase != null)
            {
                SalesRepository.Log("passPhrase received", passPhrase.ToString());
            }

            try
            {
                

                if (ignoringPassphrase == 0)
                {
                    if (passPhrase.Length <= 0 || passPhrase == null)
                    {
                        error = 1;
                    }
                    else
                    {
                        if (passPhrase.CompareTo(this.webConfigPassPhrase) != 0)
                        {
                            error = 2;
                        }
                    }
                }
                else
                {
                    error = 0;
                }
            }
            catch (NullReferenceException nrEx) {
                error = 1;
                exceptionMessage = "NullReferenceException";
            }
            catch(Exception ex){
                error = 99;
                exceptionMessage = "GeneralException";
            }

            if (error == 0)
            {
                if (!DateTime.TryParse(formDataDateTime, out this.dtTargetDate))
                {
                    error = 3;
                }
            }

            if (error > 0)
            {
                SalesRepository.Log("Error number generated", error.ToString());

                switch (error)
                {
                    case 1: dbData.error = "NoPassphrase"; break;
                    case 2: dbData.error = "InvalidPassphrase"; break;
                    case 3: dbData.error = "InvalidDate"; break;
                    case 99: dbData.error = "ServerException - " + exceptionMessage; break;
                    default: dbData.error = "UnknownError"; break;
                }

                SalesRepository.Log("Error translated", dbData.error.ToString());

                //return Json(dbData, JsonRequestBehavior.AllowGet);
            }
            else
            {
                dbData.targetDate = (string)this.dtTargetDate.ToString("yyyy/MM/dd");
                salesRepo = new SalesRepository(dbData, db);
                this.salesRepo.calculateBasicDayDataRawQuery();

                if (dbData.totalGrand <= 0)
                {
                    dbData.error = "NODATA";
                }
            }


            try
            {
                dataReturnedToClient = @"<![CDATA[<ReturnJsonPacketValues>";

                if (dbData.error != null)
                {
                    dataReturnedToClient += @"<Error>" + dbData.error.ToString() + @"</Error>";
                }

                if (dbData.nextDate != null)
                {
                    dataReturnedToClient += @"<nextDate>" + dbData.nextDate.ToString() + @"</nextDate>";
                }

                if (dbData.previousDate != null)
                {
                    dataReturnedToClient += @"<previousDate>" + dbData.previousDate.ToString() + @"</previousDate>";
                }

                if (dbData.targetDate != null)
                {
                    dataReturnedToClient += @"<targetDate>" + dbData.targetDate.ToString() + @"</targetDate>";
                }

                if (dbData.numberOfCardSales != null)
                {
                    dataReturnedToClient += @"<numberOfCardSales>" + dbData.numberOfCardSales.ToString() + @"</numberOfCardSales>";
                }

                if (dbData.numberOfCashSales != null)
                {
                    dataReturnedToClient += @"<numberOfCashSales>" + dbData.numberOfCashSales.ToString() + @"</numberOfCashSales>";
                }

                if (dbData.numberOfChargeSales != null)
                {
                    dataReturnedToClient += @"<numberOfChargeSales>" + dbData.numberOfChargeSales.ToString() + @"</numberOfChargeSales>";
                }

                if (dbData.numberOfChequeSales != null)
                {
                    dataReturnedToClient += @"<numberOfChequeSales>" + dbData.numberOfChequeSales.ToString() + @"</numberOfChequeSales>";
                }

                if (dbData.numberOfLoyaltySales != null)
                {
                    dataReturnedToClient += @"<numberOfLoyaltySales>" + dbData.numberOfLoyaltySales.ToString() + @"</numberOfLoyaltySales>";
                }

                if (dbData.totalCard != null)
                {
                    dataReturnedToClient += @"<totalCard>" + dbData.totalCard.ToString() + @"</totalCard>";
                }

                if (dbData.totalCash != null)
                {
                    dataReturnedToClient += @"<totalCash>" + dbData.totalCash.ToString() + @"</totalCash>";
                }

                if (dbData.totalCharge != null)
                {
                    dataReturnedToClient += @"<totalCharge>" + dbData.totalCharge.ToString() + @"</totalCharge>";
                }

                if (dbData.totalCheque != null)
                {
                    dataReturnedToClient += @"<totalCheque>" + dbData.totalCheque.ToString() + @"</totalCheque>";
                }

                if (dbData.totalGrand != null)
                {
                    dataReturnedToClient += @"<totalGrand>" + dbData.totalGrand.ToString() + @"</totalGrand>";
                }

                if (dbData.totalLoyalty != null)
                {
                    dataReturnedToClient += @"<totalLoyalty>" + dbData.totalLoyalty.ToString() + @"</totalLoyalty>";
                }

                dataReturnedToClient += @"</ReturnJsonPacketValues>]]>";
            }
            catch (Exception ex)
            {

            }

            try
            {
                SalesRepository.Log("DataReturnedToClient", dataReturnedToClient.ToString());
            }
            catch (Exception ex)
            {
                SalesRepository.Log("SalesRepository.Log Expection", ex.Message.ToString());
            }

            return Json(dbData, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="form">FormCollection</param>
        /// <returns>JsonResult</returns>
        [HttpGet]
        public JsonResult Month(string d, string p)
        {
            SalesRepository.Log("New request", "Request type is Month Data");
            String dataReturnedToClient = @"";

            string dateDay = this.RouteData.Values["d"].ToString();
            string formDataDateTime = dateDay.Replace("-", "/");

            string passPhrase = null;
            int error = 0;
            string exceptionMessage = null;

            SalesRepository.Log("dateDay", dateDay.ToString());

            try
            {
                passPhrase = this.HttpContext.Request.Headers["p"];
                SalesRepository.Log("passPhrase received", passPhrase.ToString());

                if (ignoringPassphrase == 0)
                {
                    if (passPhrase.Length <= 0 || passPhrase == null)
                    {
                        error = 1;
                    }
                    else
                    {
                        if (passPhrase.CompareTo(this.webConfigPassPhrase) != 0)
                        {
                            error = 2;
                        }
                    }
                }
                else
                {
                    error = 0;
                }
            }
            catch (NullReferenceException nrEx)
            {
                error = 1;
                exceptionMessage = "NullReferenceException";
            }
            catch (Exception ex)
            {
                error = 99;
                exceptionMessage = "GeneralException";
            }

            if (error == 0)
            {
                if (!DateTime.TryParse(formDataDateTime, out this.dtTargetDate))
                {
                    error = 3;
                }
            }

            MonthData dbData = new MonthData(DateTime.DaysInMonth(this.dtTargetDate.Year, this.dtTargetDate.Month));

            if (error > 0)
            {
                SalesRepository.Log("Error number generated", error.ToString());

                switch (error)
                {
                    case 1: dbData.error = "NoPassphrase"; break;
                    case 2: dbData.error = "InvalidPassphrase"; break;
                    case 3: dbData.error = "InvalidDate"; break;
                    case 99: dbData.error = "ServerException - " + exceptionMessage; break;
                    default: dbData.error = "UnknownError"; break;
                }

                SalesRepository.Log("Error translated", dbData.error.ToString());

                //return Json(dbData, JsonRequestBehavior.AllowGet);
            }
            else
            {
                dbData.monthName = new DateTime(this.dtTargetDate.Year, this.dtTargetDate.Month, this.dtTargetDate.Day).ToString("MMM");
                dbData.targetDate = (string)this.dtTargetDate.ToString("yyyy/MM/dd");
                salesRepo = new SalesRepository(dbData, db);
                this.salesRepo.calculateBasicMonthDataRawQuery();
            }

            try
            {
                dataReturnedToClient = @"<![CDATA[<ReturnJsonPacketValues>";

                if (dbData.error != null)
                {
                    dataReturnedToClient += @"<Error>" + dbData.error.ToString() + @"</Error>";
                }

                if (dbData.nextDate != null)
                {
                    dataReturnedToClient += @"<nextDate>" + dbData.nextDate.ToString() + @"</nextDate>";
                }

                if (dbData.previousDate != null)
                {
                    dataReturnedToClient += @"<previousDate>" + dbData.previousDate.ToString() + @"</previousDate>";
                }

                if (dbData.targetDate != null)
                {
                    dataReturnedToClient += @"<targetDate>" + dbData.targetDate.ToString() + @"</targetDate>";
                }

                if (dbData.numberOfCardSales != null)
                {
                    dataReturnedToClient += @"<numberOfCardSales>" + dbData.numberOfCardSales.ToString() + @"</numberOfCardSales>";
                }

                if (dbData.numberOfCashSales != null)
                {
                    dataReturnedToClient += @"<numberOfCashSales>" + dbData.numberOfCashSales.ToString() + @"</numberOfCashSales>";
                }

                if (dbData.numberOfChargeSales != null)
                {
                    dataReturnedToClient += @"<numberOfChargeSales>" + dbData.numberOfChargeSales.ToString() + @"</numberOfChargeSales>";
                }

                if (dbData.numberOfChequeSales != null)
                {
                    dataReturnedToClient += @"<numberOfChequeSales>" + dbData.numberOfChequeSales.ToString() + @"</numberOfChequeSales>";
                }

                if (dbData.numberOfLoyaltySales != null)
                {
                    dataReturnedToClient += @"<numberOfLoyaltySales>" + dbData.numberOfLoyaltySales.ToString() + @"</numberOfLoyaltySales>";
                }

                if (dbData.totalCard != null)
                {
                    dataReturnedToClient += @"<totalCard>" + dbData.totalCard.ToString() + @"</totalCard>";
                }

                if (dbData.totalCash != null)
                {
                    dataReturnedToClient += @"<totalCash>" + dbData.totalCash.ToString() + @"</totalCash>";
                }

                if (dbData.totalCharge != null)
                {
                    dataReturnedToClient += @"<totalCharge>" + dbData.totalCharge.ToString() + @"</totalCharge>";
                }

                if (dbData.totalCheque != null)
                {
                    dataReturnedToClient += @"<totalCheque>" + dbData.totalCheque.ToString() + @"</totalCheque>";
                }

                if (dbData.totalGrand != null)
                {
                    dataReturnedToClient += @"<totalGrand>" + dbData.totalGrand.ToString() + @"</totalGrand>";
                }

                if (dbData.totalLoyalty != null)
                {
                    dataReturnedToClient += @"<totalLoyalty>" + dbData.totalLoyalty.ToString() + @"</totalLoyalty>";
                }

                dataReturnedToClient += @"</ReturnJsonPacketValues>]]>";
            }
            catch (Exception ex)
            {

            }

            try
            {
                SalesRepository.Log("DataReturnedToClient", dataReturnedToClient.ToString());
            }
            catch (Exception ex)
            {
                SalesRepository.Log("SalesRepository.Log Expection", ex.Message.ToString());
            }

            return Json(dbData, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="form">FormCollection</param>
        /// <returns>JsonResult</returns>
        [HttpGet]
        public JsonResult Year(string d, string p)
        {
            YearData dbData = new YearData();

            string dateDay = this.RouteData.Values["d"].ToString();
            string formDataDateTime = dateDay.Replace("-", "/");
            String dataReturnedToClient = @"";

            string passPhrase = null;
            int error = 0;
            string exceptionMessage = null;

            try
            {
                passPhrase = this.HttpContext.Request.Headers["p"];
                SalesRepository.Log("passPhrase received", passPhrase.ToString());

                if (ignoringPassphrase == 0)
                {
                    if (passPhrase.Length <= 0 || passPhrase == null)
                    {
                        error = 1;
                    }
                    else
                    {
                        if (passPhrase.CompareTo(this.webConfigPassPhrase) != 0)
                        {
                            error = 2;
                        }
                    }
                }
                else
                {
                    error = 0;
                }
            }
            catch (NullReferenceException nrEx)
            {
                error = 1;
                exceptionMessage = "NullReferenceException";
            }
            catch (Exception ex)
            {
                error = 99;
                exceptionMessage = "GeneralException";
            }

            if (error == 0)
            {
                if (!DateTime.TryParse(formDataDateTime, out this.dtTargetDate))
                {
                    error = 3;
                }
            }

            if (error > 0)
            {
                SalesRepository.Log("Error number generated", error.ToString());

                switch (error)
                {
                    case 1: dbData.error = "NoPassphrase"; break;
                    case 2: dbData.error = "InvalidPassphrase"; break;
                    case 3: dbData.error = "InvalidDate"; break;
                    case 99: dbData.error = "ServerException - " + exceptionMessage; break;
                    default: dbData.error = "UnknownError"; break;
                }

                SalesRepository.Log("Error translated", dbData.error.ToString());

                //return Json(dbData, JsonRequestBehavior.AllowGet);
            }
            else
            {
                dbData.targetDate = (string)this.dtTargetDate.ToString("yyyy/MM/dd");
                salesRepo = new SalesRepository(dbData, db);
                this.salesRepo.calculateBasicYearDataRawQuery();
            }

            try
            {
                dataReturnedToClient = @"<![CDATA[<ReturnJsonPacketValues>";

                if (dbData.error != null)
                {
                    dataReturnedToClient += @"<Error>" + dbData.error.ToString() + @"</Error>";
                }

                if (dbData.nextDate != null)
                {
                    dataReturnedToClient += @"<nextDate>" + dbData.nextDate.ToString() + @"</nextDate>";
                }

                if (dbData.previousDate != null)
                {
                    dataReturnedToClient += @"<previousDate>" + dbData.previousDate.ToString() + @"</previousDate>";
                }

                if (dbData.targetDate != null)
                {
                    dataReturnedToClient += @"<targetDate>" + dbData.targetDate.ToString() + @"</targetDate>";
                }

                if (dbData.numberOfCardSales != null)
                {
                    dataReturnedToClient += @"<numberOfCardSales>" + dbData.numberOfCardSales.ToString() + @"</numberOfCardSales>";
                }

                if (dbData.numberOfCashSales != null)
                {
                    dataReturnedToClient += @"<numberOfCashSales>" + dbData.numberOfCashSales.ToString() + @"</numberOfCashSales>";
                }

                if (dbData.numberOfChargeSales != null)
                {
                    dataReturnedToClient += @"<numberOfChargeSales>" + dbData.numberOfChargeSales.ToString() + @"</numberOfChargeSales>";
                }

                if (dbData.numberOfChequeSales != null)
                {
                    dataReturnedToClient += @"<numberOfChequeSales>" + dbData.numberOfChequeSales.ToString() + @"</numberOfChequeSales>";
                }

                if (dbData.numberOfLoyaltySales != null)
                {
                    dataReturnedToClient += @"<numberOfLoyaltySales>" + dbData.numberOfLoyaltySales.ToString() + @"</numberOfLoyaltySales>";
                }

                if (dbData.totalCard != null)
                {
                    dataReturnedToClient += @"<totalCard>" + dbData.totalCard.ToString() + @"</totalCard>";
                }

                if (dbData.totalCash != null)
                {
                    dataReturnedToClient += @"<totalCash>" + dbData.totalCash.ToString() + @"</totalCash>";
                }

                if (dbData.totalCharge != null)
                {
                    dataReturnedToClient += @"<totalCharge>" + dbData.totalCharge.ToString() + @"</totalCharge>";
                }

                if (dbData.totalCheque != null)
                {
                    dataReturnedToClient += @"<totalCheque>" + dbData.totalCheque.ToString() + @"</totalCheque>";
                }

                if (dbData.totalGrand != null)
                {
                    dataReturnedToClient += @"<totalGrand>" + dbData.totalGrand.ToString() + @"</totalGrand>";
                }

                if (dbData.totalLoyalty != null)
                {
                    dataReturnedToClient += @"<totalLoyalty>" + dbData.totalLoyalty.ToString() + @"</totalLoyalty>";
                }

                dataReturnedToClient += @"</ReturnJsonPacketValues>]]>";
            }
            catch (Exception ex)
            {

            }

            try
            {
                SalesRepository.Log("DataReturnedToClient", dataReturnedToClient.ToString());
            }
            catch (Exception ex)
            {
                SalesRepository.Log("SalesRepository.Log Expection", ex.Message.ToString());
            }

            return Json(dbData, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Gets basic data for Day related data
        /// </summary>
        /// <param name="dateTime">DateTime</param>
        /// <returns>DayBasicData</returns>
        private DayData getDayDataTabular()
        {
            return this.salesRepo.DayBasicData;
        }

        private MonthData getMonthDataTabular()
        {
            return this.salesRepo.MonthBasicData;
        }  
    }
}