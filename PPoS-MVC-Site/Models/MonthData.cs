﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.Web.Script.Serialization;
using System.Data.Objects;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;

namespace PPoS_MVC_Site.Models
{
    [Serializable]
    public class MonthData : SalesData
    {
        private int _numDaysInMonth;
        private string _monthName;

        #region Properties

        public override string error
        {
            get { return _error; }
            set { _error = value; }
        }

        public string monthName
        {
            get { return _monthName; }
            set { _monthName = value; }
        }

        public int numDaysInMonth
        {
            get { return _numDaysInMonth; }
            set { _numDaysInMonth = value; }
        }

        public override string targetDate
        {
            get { return _targetDate; }
            set { _targetDate = value; }
        }

        public override string previousDate
        {
            get { return _previousDate; }
            set { _previousDate = value; }
        }

        public override string nextDate
        {
            get { return _nextDate; }
            set { _nextDate = value; }
        }

        public override int numberOfCashSales
        {
            get { return _numberOfCashSales; }
            set { _numberOfCashSales = value; }
        }

        public override decimal totalCash
        {
            get { return _totalCash; }
            set { _totalCash = value; }
        }

        public override int numberOfCardSales
        {
            get { return _numberOfCardSales; }
            set { _numberOfCardSales = value; }
        }

        public override decimal totalCard
        {
            get { return _totalCard; }
            set { _totalCard = value; }
        }

        public override int numberOfChequeSales
        {
            get { return _numberOfChequeSales; }
            set { _numberOfChequeSales = value; }
        }

        public override decimal totalCheque
        {
            get { return _totalCheque; }
            set { _totalCheque = value; }
        }

        public override int numberOfLoyaltySales
        {
            get { return _numberOfLoyaltySales; }
            set { _numberOfLoyaltySales = value; }
        }

        public override decimal totalLoyalty
        {
            get { return _totalLoyalty; }
            set { _totalLoyalty = value; }
        }

        public override int numberOfChargeSales
        {
            get { return _numberOfChargeSales; }
            set { _numberOfChargeSales = value; }
        }

        public override decimal totalCharge
        {
            get { return _totalCharge; }
            set { _totalCharge = value; }
        }

        public override decimal totalGrand
        {
            get { return _totalGrand; }
            set { _totalGrand = value; }
        }

        public List<MonthSaleItem> monthlySalesFigures
        {
            get { return _monthlySalesFigures; }
            set { _monthlySalesFigures = value; }
        }

        #endregion

        private List<MonthSaleItem> _monthlySalesFigures { get; set; }

        public MonthData(int numDays)
        {
            DateTime local;
            DateTime.TryParse(this.targetDate, out local);

            this.numDaysInMonth = numDays;
            _monthlySalesFigures = new List<MonthSaleItem>();

            for (int i = 1; i <= this.numDaysInMonth; i++)
            {
                this._monthlySalesFigures.Add(new MonthSaleItem(i));
            }
        }
    }
}