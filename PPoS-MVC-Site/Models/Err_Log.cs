//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PPoS_MVC_Site.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Err_Log
    {
        public int Line_No { get; set; }
        public Nullable<System.DateTime> Date_Time { get; set; }
        public string User_No { get; set; }
        public string Err_Number { get; set; }
        public string Description { get; set; }
        public string Source { get; set; }
        public string UserNo { get; set; }
    }
}
