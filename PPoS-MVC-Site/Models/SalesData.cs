﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PPoS_MVC_Site.Models
{
    [Serializable]
    public abstract class SalesData
    {
        protected string _error;
        public virtual string error
        {
            get { return _error; }
            set { _error = value; }
        }

        protected string _targetDate;
        public virtual string targetDate
        {
            get { return _targetDate; }
            set { _targetDate = value; }
        }

        protected string _previousDate;
        public virtual string previousDate
        {
            get { return _previousDate; }
            set { _previousDate = value; }
        }

        protected string _nextDate;
        public virtual string nextDate
        {
            get { return _nextDate; }
            set { _nextDate = value; }
        }

        protected int _numberOfCashSales;
        public virtual int numberOfCashSales
        {
            get { return _numberOfCashSales; }
            set { _numberOfCashSales = value; }
        }

        protected decimal _totalCash;
        public virtual decimal totalCash
        {
            get { return _totalCash; }
            set { _totalCash = value; }
        }

        protected int _numberOfCardSales;
        public virtual int numberOfCardSales
        {
            get { return _numberOfCardSales; }
            set { _numberOfCardSales = value; }
        }

        protected decimal _totalCard;
        public virtual decimal totalCard
        {
            get { return _totalCard; }
            set { _totalCard = value; }
        }

        protected int _numberOfChequeSales;
        public virtual int numberOfChequeSales
        {
            get { return _numberOfChequeSales; }
            set { _numberOfChequeSales = value; }
        }

        protected decimal _totalCheque;
        public virtual decimal totalCheque
        {
            get { return _totalCheque; }
            set { _totalCheque = value; }
        }

        protected int _numberOfLoyaltySales;
        public virtual int numberOfLoyaltySales
        {
            get { return _numberOfLoyaltySales; }
            set { _numberOfLoyaltySales = value; }
        }

        protected decimal _totalLoyalty;
        public virtual decimal totalLoyalty
        {
            get { return _totalLoyalty; }
            set { _totalLoyalty = value; }
        }

        protected int _numberOfChargeSales;
        public virtual int numberOfChargeSales
        {
            get { return _numberOfChargeSales; }
            set { _numberOfChargeSales = value; }
        }

        protected decimal _totalCharge;
        public virtual decimal totalCharge
        {
            get { return _totalCharge; }
            set { _totalCharge = value; }
        }

        protected decimal _totalGrand;
        public virtual decimal totalGrand
        {
            get { return _totalGrand; }
            set { _totalGrand = value; }
        }

    }
}