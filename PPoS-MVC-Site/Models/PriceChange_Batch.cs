//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PPoS_MVC_Site.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class PriceChange_Batch
    {
        public int Line_No { get; set; }
        public string Product_Code { get; set; }
        public string Description { get; set; }
        public Nullable<float> Landed_Cost { get; set; }
        public Nullable<float> Selling_Price { get; set; }
        public Nullable<float> Ave_Cost { get; set; }
        public Nullable<int> Selected { get; set; }
        public Nullable<float> New_Price { get; set; }
        public Nullable<int> Scheduled { get; set; }
        public Nullable<System.DateTime> Scheduled_Date { get; set; }
    }
}
