//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PPoS_MVC_Site.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Supplier_Accounts
    {
        public int Line_No { get; set; }
        public Nullable<System.DateTime> Date_Time { get; set; }
        public string Invoice_No { get; set; }
        public Nullable<int> Payment_No { get; set; }
        public string Transaction_Type { get; set; }
        public string Account_No { get; set; }
        public Nullable<float> Debit { get; set; }
        public Nullable<float> Credit { get; set; }
        public Nullable<float> Balance { get; set; }
        public Nullable<int> User_No { get; set; }
        public string Tender_Type { get; set; }
        public string Ref_No { get; set; }
    }
}
