//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PPoS_MVC_Site.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class fault
    {
        public int Fault_no { get; set; }
        public Nullable<System.DateTime> Date_Time { get; set; }
        public string User_No { get; set; }
        public Nullable<bool> Assistance_im { get; set; }
        public Nullable<bool> Assistance_nm { get; set; }
        public string Fault_Org { get; set; }
        public string Description { get; set; }
        public Nullable<bool> Satisfaction_Ex { get; set; }
        public Nullable<bool> Satisfaction_Gd { get; set; }
        public Nullable<bool> Satisfaction_Fa { get; set; }
        public Nullable<bool> Satisfaction_Not { get; set; }
        public string Improvement { get; set; }
    }
}
