﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.Web.Script.Serialization;
using System.Data.Objects;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;

namespace PPoS_MVC_Site.Models
{
    [Serializable]
    public class YearSaleItem { 
        public int month { get; set; }
        public int numberOfSales { get; set; }
        public decimal totalOfSales { get; set; }

        public YearSaleItem(int mnth)
        {
            this.month = mnth;
            this.numberOfSales = 0;
            this.totalOfSales = decimal.Parse("0.00");
        }
    }

    [Serializable]
    public class MonthSaleItem
    {
        public int day { get; set; }
        public int numberOfSales { get; set; }
        public decimal totalOfSales { get; set; }

        public MonthSaleItem(int dy)
        {
            this.day = dy;
            this.numberOfSales = 0;
            this.totalOfSales = decimal.Parse("0.00");
        }
    }

    [Serializable]
    public class HourSaleItem
    {
        public int hour { get; set; }
        public int numberOfSales { get; set; }
        public decimal totalOfSales { get; set; }

        public HourSaleItem(int hr) {
            this.hour = hr;
            this.numberOfSales = 0;
            this.totalOfSales = decimal.Parse("0.00");
        }
    }
}