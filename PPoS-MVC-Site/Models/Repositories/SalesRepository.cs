﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.Web.Script.Serialization;
using System.Data.Objects;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;

using System.IO;
using System.Text;

using PPoS_MVC_Site.Models;

namespace PPoS_MVC_Site.Models.Repositories
{
    public class VM_Sales_Journal
    {
        public float? Line_Total;
    }

    public class SalesRepository
    {
        private PlatinumContext platContext;
        private DayData dayData;
        private DateTime currentDate;
        private MonthData monthData;
        private YearData yearData;
        private int minMonthDay;
        private int maxMonthDay;
        private DateTime endOfMonthDate;
        private DateTime startOfMonthDate;

        /// <summary>
        /// Setup Repo for Day view
        /// </summary>
        /// <param name="dayData"></param>
        /// <param name="pc"></param>
        public SalesRepository(PPoS_MVC_Site.Models.DayData dayData, PlatinumContext pc)
        {
            this.platContext = pc;
            this.dayData = dayData;
            this.currentDate = DateTime.Now;
            this.setupDatesForDayView();
        }

        /// <summary>
        /// Setup Repo for Month view
        /// </summary>
        /// <param name="monthData"></param>
        /// <param name="pc"></param>
        public SalesRepository(PPoS_MVC_Site.Models.MonthData monthData, PlatinumContext pc)
        {
            this.minMonthDay = 1;
            this.maxMonthDay = getMaxDaysInMonth(monthData.targetDate);
            this.platContext = pc;
            this.monthData = monthData;
            this.currentDate = DateTime.Now;
            this.setupDatesForMonthView();
        }

        /// <summary>
        /// Setup Repo for Month view
        /// </summary>
        /// <param name="monthData"></param>
        /// <param name="pc"></param>
        public SalesRepository(PPoS_MVC_Site.Models.YearData yearData, PlatinumContext pc)
        {
            this.platContext = pc;
            this.yearData = yearData;
            this.currentDate = DateTime.Now;
            this.setupDatesForYearView();
        }

        #region Properties

        public DateTime StartOfMonthDate
        {
            get { return startOfMonthDate; }
            set { startOfMonthDate = value; }
        }

        public DateTime EndOfMonthDate
        {
            get { return endOfMonthDate; }
            set { endOfMonthDate = value; }
        }

        public int MinMonthDay
        {
            get { return minMonthDay; }
            set { minMonthDay = value; }
        }

        public int MaxMonthDay
        {
            get { return maxMonthDay; }
            set { maxMonthDay = value; }
        }

        public MonthData MonthBasicData
        {
            get { return monthData; }
            set { monthData = value; }
        }

        public PlatinumContext PlatContext
        {
            get { return platContext; }
            set { platContext = value; }
        }

        public DayData DayBasicData
        {
            get { return dayData; }
            set { dayData = value; }
        }

        #endregion

        /// <summary>
        /// 
        /// </summary>
        private void setupDatesForYearView()
        {
            if (DateTime.Compare(this.currentDate, DateTime.Parse(yearData.targetDate)) == 0)
            {
                yearData.nextDate = (string)this.currentDate.ToString("yyyy/MM/dd");
            }
            else
            {
                yearData.nextDate = DateTime.Parse(yearData.targetDate).AddYears(1).ToString("yyyy/MM/dd");
            }

            yearData.previousDate = DateTime.Parse(yearData.targetDate).AddYears(-1).ToString("yyyy/MM/dd");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="targetDate"></param>
        /// <returns></returns>
        private int getMaxDaysInMonth(string targetDate)
        {
            DateTime targetDateAsDateTime;
            DateTime.TryParse(targetDate, out targetDateAsDateTime);

            return DateTime.DaysInMonth(targetDateAsDateTime.Year, targetDateAsDateTime.Month);
        }

        /// <summary>
        /// Using target date we work out what the next and previous monthly date is for the clients navigation
        /// </summary>
        private void setupDatesForMonthView()
        {
            if (DateTime.Compare(this.currentDate, DateTime.Parse(monthData.targetDate)) == 0)
            {
                monthData.nextDate = (string)this.currentDate.ToString("yyyy/MM/dd");
            }
            else
            {
                monthData.nextDate = DateTime.Parse(monthData.targetDate).AddMonths(1).ToString("yyyy/MM/dd");
            }

            monthData.previousDate = DateTime.Parse(monthData.targetDate).AddMonths(-1).ToString("yyyy/MM/dd");
        }

        /// <summary>
        /// Using target date we work out what the next and previous date is for the clients navigation
        /// </summary>
        private void setupDatesForDayView()
        {
            if (DateTime.Compare(this.currentDate, DateTime.Parse(dayData.targetDate)) == 0)
            {
                dayData.nextDate = (string)this.currentDate.ToString("yyyy/MM/dd");
            }
            else
            {
                dayData.nextDate = DateTime.Parse(dayData.targetDate).AddDays(1).ToString("yyyy/MM/dd");
            }

            dayData.previousDate = DateTime.Parse(dayData.targetDate).AddDays(-1).ToString("yyyy/MM/dd");
        }

        public void getMonthlyData()
        {
            foreach (MonthSaleItem item in monthData.monthlySalesFigures)
            {
                DateTime localTargetDate;
                DateTime localDateForLoop;

                int? currentDay = null;
                int? month = null;
                int? year = null;

                DateTime.TryParse(monthData.targetDate, out localTargetDate);
                currentDay = item.day;
                month = localTargetDate.Month;
                year = localTargetDate.Year;

                DateTime.TryParse(year.ToString() + "/" + ((month < 10) ? ("0" + month.ToString()) : (month.ToString())) + "/" + currentDay.ToString(), out localDateForLoop);

                string targetStartTime = "00:00:00 AM";
                string targetEndTime = "23:59:59 PM";

                string sSqlAllSales = @"SELECT * FROM Sales_Journal 
                                        WHERE (Function_Key = 9 OR Function_Key = 10 OR Function_Key = 11 OR 
                                        Function_Key = 12 OR Function_Key = 13) AND 
                                        Date_Time >= '" + localDateForLoop.ToString("yyyy/MM/dd") + " " + targetStartTime + "' AND " +
                                        @"Date_Time <= '" + localDateForLoop.ToString("yyyy/MM/dd") + " " + targetEndTime + "'";

                var sJournalCash = platContext.Sales_Journal.SqlQuery(sSqlAllSales);

                item.totalOfSales = 0;
                item.numberOfSales = 0;

                foreach (var salejournal in sJournalCash)
                {
                    item.totalOfSales += (salejournal.Line_Total != null) ? ((decimal)salejournal.Line_Total) : 0;
                    item.numberOfSales++;
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public void getHourlyData()
        {
            Log("SalesRepository Class", "getHourlyData() called");

            foreach (HourSaleItem item in dayData.hourlySalesFigures)
            {
                int currentHour = item.hour;
                string timePart = (currentHour < 12)?"AM":"PM";

                string targetStartTime = (currentHour <= 9) ? ("0" + currentHour.ToString() + ":00:00 " + timePart) : (currentHour.ToString() + ":00:00 " + timePart);
                string targetEndTime = (currentHour <= 9) ? ("0" + currentHour.ToString() + ":59:59 " + timePart) : (currentHour.ToString() + ":59:59 " + timePart);

                string sSqlAllSales = @"SELECT * FROM Sales_Journal 
                                        WHERE (Function_Key = 9 OR Function_Key = 10 OR Function_Key = 11 OR 
                                        Function_Key = 12 OR Function_Key = 13) AND 
                                        Date_Time >= '" + DateTime.Parse(dayData.targetDate).Date.ToString("yyyy/MM/dd") + " " + targetStartTime + "' AND " +
                                        @"Date_Time <= '" + DateTime.Parse(dayData.targetDate).Date.ToString("yyyy/MM/dd") + " " + targetEndTime + "'";
                var sJournalCash = platContext.Sales_Journal.SqlQuery(sSqlAllSales);

                item.totalOfSales = 0;
                item.numberOfSales = 0;

                foreach (var salejournal in sJournalCash)
                {
                    item.totalOfSales += (salejournal.Line_Total != null) ? ((decimal)salejournal.Line_Total) : 0;
                    item.numberOfSales++;
                }
            }
          
        }

        public static void Log(string msgid, string msg)
        {
            const bool debugEnabled = false;

            if (debugEnabled)
            {
                System.Xml.XmlDocument doc = new System.Xml.XmlDocument();

                if (System.IO.File.Exists(@"D:\Development\MS-MVC\Network Associates\PPoS-MVC-Published\logs\logs.xml"))
                    doc.Load(@"D:\Development\MS-MVC\Network Associates\PPoS-MVC-Published\logs\logs.xml");
                else
                {
                    var root = doc.CreateElement("logs");
                    doc.AppendChild(root);
                }

                var el = (System.Xml.XmlElement)doc.DocumentElement.AppendChild(doc.CreateElement("logs"));
                el.SetAttribute("MessageId", msgid);
                el.AppendChild(doc.CreateElement("Message")).InnerText = msg;
                doc.Save(@"D:\Development\MS-MVC\Network Associates\PPoS-MVC-Published\logs\logs.xml");
            }
        }

        public void calculateBasicMonthDataRawQuery()
        {
            DateTime localTargetDate;
            DateTime.TryParse(monthData.targetDate, out localTargetDate);

            DateTime.TryParse(("1 " + localTargetDate.ToString("MMM") + " " + localTargetDate.Year), out startOfMonthDate);
            DateTime.TryParse((this.MaxMonthDay + " " + localTargetDate.ToString("MMM") + " " + localTargetDate.Year), out endOfMonthDate);

            string sqlDateRange = "CONVERT(DATETIME, FLOOR(CONVERT(FLOAT, Date_Time))) >= '" + this.StartOfMonthDate.ToString("yyyy/MM/dd") + "' AND CONVERT(DATETIME, FLOOR(CONVERT(FLOAT, Date_Time))) <= '" + this.EndOfMonthDate.ToString("yyyy/MM/dd") + "'";

            string sSqlCash = "SELECT * FROM Sales_Journal WHERE Function_Key = 9 AND " + sqlDateRange;
            string sSqlCard = "SELECT * FROM Sales_Journal WHERE Function_Key = 10 AND " + sqlDateRange;
            string sSqlCheque = "SELECT * FROM Sales_Journal WHERE Function_Key = 11 AND " + sqlDateRange;
            string sSqlCharge = "SELECT * FROM Sales_Journal WHERE Function_Key = 12 AND " + sqlDateRange;
            string sSqlLoyalty = "SELECT * FROM Sales_Journal WHERE Function_Key = 13 AND " + sqlDateRange;

            Log("sSqlCash", "<![CDATA[" + sSqlCash + "]]>");

            var sJournalCash = platContext.Sales_Journal.SqlQuery(sSqlCash);

            monthData.numberOfCashSales = sJournalCash.Count();
            monthData.totalCash = this.calculateDayTotalSales(sJournalCash);

            var sJournalCard = platContext.Sales_Journal.SqlQuery(sSqlCard);

            monthData.numberOfCardSales = sJournalCard.Count();
            monthData.totalCard = this.calculateDayTotalSales(sJournalCard);

            var sJournalCheque = platContext.Sales_Journal.SqlQuery(sSqlCheque);

            monthData.numberOfChequeSales = sJournalCheque.Count();
            monthData.totalCheque = this.calculateDayTotalSales(sJournalCheque);

            var sJournalCharge = platContext.Sales_Journal.SqlQuery(sSqlCharge);

            monthData.numberOfChargeSales = sJournalCharge.Count();
            monthData.totalCharge = this.calculateDayTotalSales(sJournalCharge);

            var sJournalLoyalty = platContext.Sales_Journal.SqlQuery(sSqlLoyalty);

            monthData.numberOfLoyaltySales = sJournalLoyalty.Count();
            monthData.totalLoyalty = this.calculateDayTotalSales(sJournalLoyalty);

            monthData.totalGrand = monthData.totalCash + monthData.totalCard + monthData.totalCheque + monthData.totalCharge + monthData.totalLoyalty;

            this.getMonthlyData();
        }

        /// <summary>
        /// Gets the basic daily figures.
        /// </summary>
        public void calculateBasicDayDataRawQuery()
        {

            string sSqlCash = "SELECT * FROM Sales_Journal WHERE Function_Key = 9 AND CONVERT(DATETIME, FLOOR(CONVERT(FLOAT, Date_Time))) = '" + dayData.targetDate.ToString() + "'";
            //string sSqlCashCount = "SELECT COUNT(*) FROM Sales_Journal WHERE Function_Key = 9 AND CONVERT(DATETIME, FLOOR(CONVERT(FLOAT, Date_Time))) = '" + dayData.TargetDate.ToString() + "'";
            string sSqlCard = "SELECT * FROM Sales_Journal WHERE Function_Key = 10 AND CONVERT(DATETIME, FLOOR(CONVERT(FLOAT, Date_Time))) = '" + dayData.targetDate.ToString() + "'";
            //string sSqlCardCount = "SELECT COUNT(*) FROM Sales_Journal WHERE Function_Key = 10 AND CONVERT(DATETIME, FLOOR(CONVERT(FLOAT, Date_Time))) = '" + dayData.TargetDate.ToString() + "'";
            string sSqlCheque = "SELECT * FROM Sales_Journal WHERE Function_Key = 11 AND CONVERT(DATETIME, FLOOR(CONVERT(FLOAT, Date_Time))) = '" + dayData.targetDate.ToString() + "'";
            //string sSqlChequeCount = "SELECT COUNT(*) FROM Sales_Journal WHERE Function_Key = 11 AND CONVERT(DATETIME, FLOOR(CONVERT(FLOAT, Date_Time))) = '" + dayData.TargetDate.ToString() + "'";
            string sSqlCharge = "SELECT * FROM Sales_Journal WHERE Function_Key = 12 AND CONVERT(DATETIME, FLOOR(CONVERT(FLOAT, Date_Time))) = '" + dayData.targetDate.ToString() + "'";
            //string sSqlChargeCount = "SELECT COUNT(*) FROM Sales_Journal WHERE Function_Key = 12 AND CONVERT(DATETIME, FLOOR(CONVERT(FLOAT, Date_Time))) = '" + dayData.TargetDate.ToString() + "'";
            string sSqlLoyalty = "SELECT * FROM Sales_Journal WHERE Function_Key = 13 AND CONVERT(DATETIME, FLOOR(CONVERT(FLOAT, Date_Time))) = '" + dayData.targetDate.ToString() + "'";
            //string sSqlLoyaltyCount = "SELECT COUNT(*) FROM Sales_Journal WHERE Function_Key = 13 AND CONVERT(DATETIME, FLOOR(CONVERT(FLOAT, Date_Time))) = '" + dayData.TargetDate.ToString() + "'";

            Log("calculateBasicDayDataRawQuery-sSqlCash", "<![CDATA[" + sSqlCash + "]]>");

            var sJournalCash = platContext.Sales_Journal.SqlQuery(sSqlCash);

            dayData.numberOfCashSales = sJournalCash.Count();
            dayData.totalCash = this.calculateDayTotalSales(sJournalCash);

            var sJournalCard = platContext.Sales_Journal.SqlQuery(sSqlCard);

            dayData.numberOfCardSales = sJournalCard.Count();
            dayData.totalCard = this.calculateDayTotalSales(sJournalCard);

            var sJournalCheque = platContext.Sales_Journal.SqlQuery(sSqlCheque);

            dayData.numberOfChequeSales = sJournalCheque.Count();
            dayData.totalCheque = this.calculateDayTotalSales(sJournalCheque);

            var sJournalCharge = platContext.Sales_Journal.SqlQuery(sSqlCharge);

            dayData.numberOfChargeSales = sJournalCharge.Count();
            dayData.totalCharge = this.calculateDayTotalSales(sJournalCharge);

            var sJournalLoyalty = platContext.Sales_Journal.SqlQuery(sSqlLoyalty);

            dayData.numberOfLoyaltySales = sJournalLoyalty.Count();
            dayData.totalLoyalty = this.calculateDayTotalSales(sJournalLoyalty);

            dayData.totalGrand = dayData.totalCash + dayData.totalCard + dayData.totalCheque + dayData.totalCharge + dayData.totalLoyalty;

            this.getHourlyData();
        }

        /// <summary>
        /// Sums all totals for a grand total for the months sales
        /// </summary>
        /// <param name="sj"></param>
        /// <returns></returns>
        private decimal calculateMonthTotalSales(System.Data.Entity.Infrastructure.DbSqlQuery<Sales_Journal> sj)
        {
            decimal cashtotal = 0;

            foreach (var jItem in sj.ToList())
            {
                float? lineTotal = jItem.Line_Total;
                if (lineTotal != null)
                {
                    cashtotal += decimal.Parse(lineTotal.ToString());
                }
            }

            return cashtotal;
        }

        /// <summary>
        /// Sums all totals for a grand total for the days sales
        /// </summary>
        /// <param name="sj">System.Data.Entity.Infrastructure.DbSqlQuery<Sales_Journal></param>
        /// <returns>decimal</returns>
        private decimal calculateDayTotalSales(System.Data.Entity.Infrastructure.DbSqlQuery<Sales_Journal> sj)
        {
            decimal cashtotal = 0;

            foreach (var jItem in sj.ToList())
            {
                float? lineTotal = jItem.Line_Total;
                if (lineTotal != null)
                {
                    cashtotal += decimal.Parse(lineTotal.ToString());
                }
            }

            return cashtotal;
        }

        internal void calculateBasicYearDataRawQuery()
        {
            DateTime localTargetDate;
            DateTime.TryParse(yearData.targetDate, out localTargetDate);
            
            DateTime startOfYearDate;
            DateTime.TryParse(("01 Jan " + localTargetDate.Year), out startOfYearDate);

            DateTime endOfYearDate;
            DateTime.TryParse(("31 Dec " + localTargetDate.Year), out endOfYearDate);

            string sqlDateRange = "CONVERT(DATETIME, FLOOR(CONVERT(FLOAT, Date_Time))) >= '" + startOfYearDate.ToString("yyyy/MM/dd") + "' AND CONVERT(DATETIME, FLOOR(CONVERT(FLOAT, Date_Time))) <= '" + endOfYearDate.ToString("yyyy/MM/dd") + "'";

            string sSqlCash = "SELECT * FROM Sales_Journal WHERE Function_Key = 9 AND " + sqlDateRange;
            string sSqlCard = "SELECT * FROM Sales_Journal WHERE Function_Key = 10 AND " + sqlDateRange;
            string sSqlCheque = "SELECT * FROM Sales_Journal WHERE Function_Key = 11 AND " + sqlDateRange;
            string sSqlCharge = "SELECT * FROM Sales_Journal WHERE Function_Key = 12 AND " + sqlDateRange;
            string sSqlLoyalty = "SELECT * FROM Sales_Journal WHERE Function_Key = 13 AND " + sqlDateRange;

            // D:\Development\MS-MVC\Network Associates\PPoS-MVC-Site\PPoS-MVC-Site\logs\log.txt
            // ErrorLog(@"D:\Development\MS-MVC\Network Associates\PPoS-MVC-Site\PPoS-MVC-Site\logs\log.txt", "sSqlCash: " + sSqlCash);
            Log("sSqlCash", "<![CDATA[" + sSqlCash + "]]>");

            var sJournalCash = platContext.Sales_Journal.SqlQuery(sSqlCash);

            yearData.numberOfCashSales = sJournalCash.Count();
            yearData.totalCash = this.calculateDayTotalSales(sJournalCash);

            var sJournalCard = platContext.Sales_Journal.SqlQuery(sSqlCard);

            yearData.numberOfCardSales = sJournalCard.Count();
            yearData.totalCard = this.calculateDayTotalSales(sJournalCard);

            var sJournalCheque = platContext.Sales_Journal.SqlQuery(sSqlCheque);

            yearData.numberOfChequeSales = sJournalCheque.Count();
            yearData.totalCheque = this.calculateDayTotalSales(sJournalCheque);

            var sJournalCharge = platContext.Sales_Journal.SqlQuery(sSqlCharge);

            yearData.numberOfChargeSales = sJournalCharge.Count();
            yearData.totalCharge = this.calculateDayTotalSales(sJournalCharge);

            var sJournalLoyalty = platContext.Sales_Journal.SqlQuery(sSqlLoyalty);

            yearData.numberOfLoyaltySales = sJournalLoyalty.Count();
            yearData.totalLoyalty = this.calculateDayTotalSales(sJournalLoyalty);

            yearData.totalGrand = yearData.totalCash + yearData.totalCard + yearData.totalCheque + yearData.totalCharge + yearData.totalLoyalty;

            this.getYearlyData();
        }

        private void getYearlyData()
        {
            foreach (YearSaleItem item in yearData.yearlySalesFigures)
            {
                DateTime localTargetDate;
                DateTime localStartDate;
                DateTime localEndDate;

                int? currentMonth = null;
                int? month = null;
                int? year = null;

                DateTime.TryParse(yearData.targetDate, out localTargetDate);                
                currentMonth = item.month;
                string currentMonthPadded = ((currentMonth < 10) ? ("0" + currentMonth.ToString()) : (currentMonth.ToString()));
                year = localTargetDate.Year;
                int numDaysInMonth = DateTime.DaysInMonth(localTargetDate.Year, localTargetDate.Month);

                DateTime.TryParse(year.ToString() + "/" + currentMonthPadded + "/01", out localStartDate);
                localEndDate = localStartDate.AddDays(numDaysInMonth-1);

                string targetStartTime = "00:00:00 AM";
                string targetEndTime = "23:59:59 PM";

                string sSqlAllSales = @"SELECT * FROM Sales_Journal 
                                        WHERE (Function_Key = 9 OR Function_Key = 10 OR Function_Key = 11 OR 
                                        Function_Key = 12 OR Function_Key = 13) AND 
                                        Date_Time >= '" + localStartDate.ToString("yyyy/MM/dd") + " " + targetStartTime + "' AND " +
                                        @"Date_Time <= '" + localEndDate.ToString("yyyy/MM/dd") + " " + targetEndTime + "'";

                var sJournalCash = platContext.Sales_Journal.SqlQuery(sSqlAllSales);

                item.totalOfSales = 0;
                item.numberOfSales = 0;

                foreach (var salejournal in sJournalCash)
                {
                    item.totalOfSales += (salejournal.Line_Total != null) ? ((decimal)salejournal.Line_Total) : 0;
                    item.numberOfSales++;
                }
            }
        }

        public void ErrorLog(string sPathName, string sErrMsg)
        {
            StreamWriter sw = new StreamWriter(sPathName, true);
            sw.WriteLine(sErrMsg);
            sw.Flush();
            sw.Close();
        }
    }
}