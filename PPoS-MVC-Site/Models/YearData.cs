﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PPoS_MVC_Site.Models
{
    public class YearData : SalesData
    {
        private const int NUMMONTHSINYEAR = 12;

        private List<YearSaleItem> _yearlySalesFigures { get; set; }

        public YearData() {
            DateTime local;
            DateTime.TryParse(this.targetDate, out local);
            _yearlySalesFigures = new List<YearSaleItem>();

            for (int i = 1; i <= NUMMONTHSINYEAR; i++)
            {
                this._yearlySalesFigures.Add(new YearSaleItem(i));
            }
        }

        #region Properties

        public override string error
        {
            get { return _error; }
            set { _error = value; }
        }

        public override string targetDate
        {
            get { return _targetDate; }
            set { _targetDate = value; }
        }

        public override string previousDate
        {
            get { return _previousDate; }
            set { _previousDate = value; }
        }

        public override string nextDate
        {
            get { return _nextDate; }
            set { _nextDate = value; }
        }

        public override int numberOfCashSales
        {
            get { return _numberOfCashSales; }
            set { _numberOfCashSales = value; }
        }

        public override decimal totalCash
        {
            get { return _totalCash; }
            set { _totalCash = value; }
        }

        public override int numberOfCardSales
        {
            get { return _numberOfCardSales; }
            set { _numberOfCardSales = value; }
        }

        public override decimal totalCard
        {
            get { return _totalCard; }
            set { _totalCard = value; }
        }

        public override int numberOfChequeSales
        {
            get { return _numberOfChequeSales; }
            set { _numberOfChequeSales = value; }
        }

        public override decimal totalCheque
        {
            get { return _totalCheque; }
            set { _totalCheque = value; }
        }

        public override int numberOfLoyaltySales
        {
            get { return _numberOfLoyaltySales; }
            set { _numberOfLoyaltySales = value; }
        }

        public override decimal totalLoyalty
        {
            get { return _totalLoyalty; }
            set { _totalLoyalty = value; }
        }

        public override int numberOfChargeSales
        {
            get { return _numberOfChargeSales; }
            set { _numberOfChargeSales = value; }
        }

        public override decimal totalCharge
        {
            get { return _totalCharge; }
            set { _totalCharge = value; }
        }

        public override decimal totalGrand
        {
            get { return _totalGrand; }
            set { _totalGrand = value; }
        }

        public List<YearSaleItem> yearlySalesFigures
        {
            get { return _yearlySalesFigures; }
            set { _yearlySalesFigures = value; }
        }

        #endregion


    }
}